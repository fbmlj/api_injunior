# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Member.create!(name:"Gyselle" ,directorship: "Gestao de Pessoas", age: 21)
Member.create!(name: "Niasi",directorship: "Gestao de Pessoas" , age: 25)
Member.create!(name: "Bruno",directorship: "Projeto", age: 20)
Member.create!(name: "Jenniffer",directorship: "Projeto" , age: 21)
Member.create!(name: "Marcelo",directorship: "Comercial" , age: 21)
Member.create!(name: "Camille",directorship: "Comercial", age: 21)
Member.create!(name: "Arthur",directorship: "Operações", age: 19)
Member.create!(name: "Luisa",directorship: "Operações", age: 20)
Member.create!(name: "Lucas", directorship: "Comercial", age: 19)
